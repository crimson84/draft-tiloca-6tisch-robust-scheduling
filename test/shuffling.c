#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

/* Test configuration */

// Starting slotframe (before the first one having a shuffled schedule).
// Note: the network starts at slotframe 0, with ASN = 0.
static const unsigned long long int starting_slotframe = 0;

// Number of test rounds, as number of slotframes.
static const int rounds = 2;


/* Network configuration */

// Size of the ASN in bytes.
static const uint16_t ASN_size = 5 * sizeof(unsigned char);

// Slotframe size in timeslots.
static const uint16_t N_S = 3;

// Number of available channel offsets.
static const uint16_t N_C = 4;

// Starting ASN
static const unsigned long long int starting_ASN = starting_slotframe * N_S;

// Original timeslot scheduling (N_S elements).
// 0 = unused; 1 = Tx; 2 = Rx.
static const uint16_t timeslot_schedule[] = {1, 1, 2};

// Original channel offset scheduling (N_S elements).
// Values range from 0 to N_C, where {0, 1, ... N_C - 1} refer to actual
// channel offsets, while N_C is used where the corresponding element in
// the 'timeslot_schedule' vector is set to 0 (i.e., unused timeslot).
static const uint16_t chOff_schedule[] = {3, 1, 0};

// Channel hopping sequence (N_C elements).
static const uint16_t hopping_sequence[] = {0, 1, 2, 3};

/* Security configuration */

/* 
 * As per AES-16-64-128 , the following holds.
 * 
 * - L = 16, i.e. the AES Nonce is 13 Bytes in size (15 - L/8).
 * - M = 64, i.e. the AES Tag is 64 bits (8 Bytes) in size.
 * - k = 128, i.e. the AES Key is 128 bits (16 Bytes) in size.
 * 
 */

static const uint16_t AES_L = 2 * sizeof(unsigned char);

static const uint16_t AES_nonce_size = 13 * sizeof(unsigned char);

static const uint16_t AES_tag_size = 8 * sizeof(unsigned char);

// 8 = (13 - 5) for 13 byte nonces;
// 2 = (7 - 5)  for 7 byte nonces;
static const uint16_t AES_nonce_padding = AES_nonce_size - ASN_size;

// AES key to permute the timeslots.
static const unsigned char K_s[] = {
    0xce, 0xb0, 0x09, 0xae, 0xa4, 0x45, 0x44, 0x51,
    0xfe, 0xad, 0xf0, 0xe6, 0xb3, 0x6f, 0x45, 0x55
};

// AES key to permute the channel offsets.
static const unsigned char K_c[] = {
    0xce, 0xb0, 0x09, 0xae, 0xa4, 0x45, 0x44, 0x51,
    0xfe, 0xad, 0xf0, 0xe6, 0xb3, 0x6f, 0x45, 0x56
};

// Print the content of a byte array.
void print_octects(const unsigned char bytes[], const int size) {
    int i;
    for (i = 0; i < size; i++)
        printf("%x ", bytes[i]);
    printf("\n");
}

// Convert a uint64 in its binary representation (network byte order).
void longlongint_to_bytes(const unsigned long long int n, unsigned char* bytes)
{
    bytes[0] = (n >> 56) & 0xFF;
    bytes[1] = (n >> 48) & 0xFF;
    bytes[2] = (n >> 40) & 0xFF;
    bytes[3] = (n >> 32) & 0xFF;
    bytes[4] = (n >> 24) & 0xFF;
    bytes[5] = (n >> 16) & 0xFF;
    bytes[6] = (n >> 8) & 0xFF;
    bytes[7] = n & 0xFF;
}

// Convert the (network byte order) binary representation of a uint64 into a number.
unsigned long long int bytes_to_longlongint(const unsigned char* bytes)
{    
    unsigned long long int n = 0;
    
    n = bytes[7]
        | (unsigned long long int)bytes[6] << 8
        | (unsigned long long int)bytes[5] << 16
        | (unsigned long long int)bytes[4] << 24
        | (unsigned long long int)bytes[3] << 32
        | (unsigned long long int)bytes[2] << 40
        | (unsigned long long int)bytes[1] << 48
        | (unsigned long long int)bytes[0] << 56;
                                     
    return n;
}

void swap_elements(uint16_t *v, const int i, const int j)
{
    const uint16_t aux = v[i];
    v[i] = v[j];
    v[j] = aux;
}

void aes_ccm_encrypt(const unsigned char* ccm_pt,
                     const int ccm_pt_size,
                     const unsigned char* ccm_nonce,                     const unsigned char* ccm_key,
                     unsigned char* outbuf,
                     int* outlen)
{
    
    int tmplen;
    int ret;
    //unsigned char tag[1024];
    EVP_CIPHER_CTX *ctx;
    
    printf("\nAES CCM Encrypt:\n");

    // Debug
    /*
    printf("Plaintext:\n");
    BIO_dump_fp(stdout, ccm_pt, ccm_pt_size);
    printf("AES Key:\n");
    BIO_dump_fp(stdout, ccm_key, 16);
    printf("AES nonce:\n");
    BIO_dump_fp(stdout, ccm_nonce, AES_nonce_size);
    */
    
    ctx = EVP_CIPHER_CTX_new();
    
    // Set cipher type and mode
    ret = EVP_EncryptInit_ex(ctx, EVP_aes_128_ccm(), NULL, NULL, NULL);
    //printf("EVP_EncryptInit_ex returned %d\n", ret);
    
    // Set L
    ret = EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_CCM_SET_L, AES_L, NULL);
    //printf("EVP_CIPHER_CTX_ctrl returned %d\n", ret);
    
    // Set nonce length
    ret = EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_SET_IVLEN, AES_nonce_size, NULL);
    //printf("EVP_CIPHER_CTX_ctrl returned %d\n", ret);
    
    // Set tag length
    ret = EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_SET_TAG, AES_tag_size, NULL);
    //printf("EVP_CIPHER_CTX_ctrl returned %d\n", ret);

    // Initialise key and IV
    ret = EVP_EncryptInit_ex(ctx, NULL, NULL, ccm_key, ccm_nonce);
    //printf("EVP_EncryptInit_ex returned %d\n", ret);
    
    // Set plaintext length: only needed if AAD is used
    ret = EVP_EncryptUpdate(ctx, NULL, outlen, NULL, ccm_pt_size);
    //printf("EVP_EncryptUpdate returned %d\n", ret);

    // Encrypt plaintext: can only be called once
    ret = EVP_EncryptUpdate(ctx, outbuf, outlen, ccm_pt, ccm_pt_size);
    //printf("EVP_EncryptUpdate returned %d\n", ret);

    // Debug
    //printf("Partial ciphertext size: %d\n", *outlen);
    
    // Finalize plaintext encryption: can only be called once
    ret = EVP_EncryptFinal_ex(ctx, outbuf + (*outlen), &tmplen);
    //printf("EVP_EncryptFinal_ex returned %d\n", ret);
    
    (*outlen) += tmplen;
    
    // Debug
    // printf("Ciphertext size: %d\n", *outlen);
    
    // Print encrypted output
    printf("Ciphertext:\n");
    BIO_dump_fp(stdout, outbuf, *outlen);

    /*
    EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_GET_TAG, AES_tag_size, tag);
    // Output tag
    printf("Tag:\n");
    BIO_dump_fp(stdout, tag, AES_tag_size);
    */
    
    EVP_CIPHER_CTX_free(ctx);
}

// Pseudo-random shuffle the 'size' elements of the vector 'out'.
// Use 'k' as AEAD key and 'counter' as base nonce to generate the first pseudo-random number. The nonce value is incremented after every pseudo-random generation.
// If not null, the vector 'peer_v' has the same size of 'out' and is also shuffled, according to the same permutation used to shuffle vector 'out'.
void shuffle_vector(uint16_t *out, const int size, const unsigned char *k, unsigned long long int *counter, uint16_t *peer_v)
{
    // size of the counter (bytes)
    int counter_size = sizeof(unsigned long long int) * sizeof(unsigned char);
    
    // size of the plaintext (bytes)
    int plen = ASN_size;
    
    // size of the ciphertext (bytes)
    int clen;
    
    uint16_t aux;
    int i;
    unsigned long long int j;
    unsigned char *counter_bytes;
    unsigned char *plaintext_bytes;
    unsigned char *nonce_bytes;
    unsigned char ciphertext[1024];
    unsigned char *j_bytes;
    
    // Fisher-Yates Shuffle
    for (i = size - 1; i > 0; i--) {
        counter_bytes = malloc(counter_size);
        memset(counter_bytes, 0, counter_size);
        longlongint_to_bytes(*counter, counter_bytes);
        
        // Debug - Print the counter
        printf("Counter: %llu\n", *counter);
        printf("Counter as byte string:\n");
        BIO_dump_fp(stdout, counter_bytes, counter_size);
        
        // Build the AES nonce by padding the ASN with zeros
        nonce_bytes = malloc(AES_nonce_size);
        memset(nonce_bytes, 0, AES_nonce_size);
        memcpy(nonce_bytes + AES_nonce_padding, counter_bytes + (sizeof(uint64_t) - ASN_size), ASN_size);
        
        // Generate a psedo-random value 'j' between 0 (included) and 'i' (included)
        
        // The plaintext coincides with the least 'ASN_size' significant bytes of 'counter_bytes'
        plaintext_bytes = malloc(plen);
        memcpy(plaintext_bytes, counter_bytes + (sizeof(unsigned long long int) - ASN_size), ASN_size);
        
        // Debug - Print the plaintext
        printf("Plaintext:\n");
        BIO_dump_fp(stdout, plaintext_bytes, plen);
        
        // Debug - Print the nonce
        printf("Nonce:\n");
        BIO_dump_fp(stdout, nonce_bytes, AES_nonce_size);
        
        // Encrypt 'plaintext_bytes' as plaintext
        // Use 'k' as AES key and 'nonce_bytes' as AES nonce
        aes_ccm_encrypt(plaintext_bytes, plen, nonce_bytes, k, ciphertext, & clen);
        
        // Expand the ciphertext to an unsigned long long integer and interpret it as swapping index 'j'
        j_bytes = malloc(sizeof(unsigned long long int));
        memset(j_bytes, 0, sizeof(unsigned long long int));
        // The least significant byte of the ciphertext is at the end of 'ciphertext'
        memcpy(j_bytes + 3, ciphertext, ASN_size);
        j = bytes_to_longlongint(j_bytes) % (i + 1);
        
        // Debug - Print 'j_bytes'
        printf("\nPadded ciphertext in bytes:\n");
        BIO_dump_fp(stdout, j_bytes, sizeof(unsigned long long int));
        
        // Debug - Print swap indexes
        printf("\nSwap index i: %d\nSwap-index j: %llu\n", i, j);
        
        // Swap element 'j' with element 'i'
        swap_elements(out, i, j);
        
        // Apply the same permutation to 'peer_v'
        if (peer_v != NULL)
            swap_elements(peer_v, i, j);
        
        (*counter)++;
        free(counter_bytes);
        free(nonce_bytes);
        free(plaintext_bytes);
        free(j_bytes);
        
        printf("\n**********\n\n");
        
    }
        
}

// Shuffle the 'out' vector encoding the timeslot schedule.
// The 'counter' coincides with the ASN value at the beginning of the slotframe during which the shuffling is performed.
// Note that the resulting shuffled schedule applies to the following slotframe.
void shuffle_timeslots(uint16_t *out, const int size, unsigned long long int *counter, uint16_t * peer_v) {    
    shuffle_vector(out, size, K_s, counter, peer_v);
}

// Shuffle the 'out' vector encoding the channel offset schedule.
// The 'counter' coincides with the [N_C * floor(alt_ASN, N_S)], where alt_ASN is the ASN value at the beginning of the slotframe during which the shuffling is performed.
// Note that the resulting shuffled schedule applies to the following slotframe.
void shuffle_chOff(uint16_t *out, const int size, unsigned long long int *counter) {
    int i, j;
    uint16_t* num_sequence = malloc(N_C * sizeof(uint16_t));

    // Produce a permutation of { 0, 1, ... , N_C - 1 }
    for (i = 0; i < N_C; i++)
        num_sequence[i] = i;
    shuffle_vector(num_sequence, N_C, K_c, counter, NULL);
    
    // Update the channel offset schedule according to the above permutation
    for (i = 0; i < size; i++) {
        if (out[i] != N_C) { // N_C would indicate an unused timeslot
            j = out[i];
            out[i] = num_sequence[j];
        }
    }
    
    free(num_sequence);
}

// Print the content of the vector 'v' of size 'size'
// The vector specifies the behavior ('hint' == 0), or the chOff ('hint' == 1) to use during the i-th timeslot.
void print_schedule(const uint16_t *v, const int size, const int hint) {
    int i;
    for (i = 0; i < size; i++) {
        if (hint == 0) {
            switch (v[i]) {
                case 0:
                    printf("| -");
                    break;
                case 1:
                    printf("|Tx");
                    break;
                case 2:
                    printf("|Rx");
                    break;
            }
        }
        else if (hint == 1) {
            if (hint == 1 && v[i] == N_C) // unused timeslot
                printf("| -");
            else {
                printf("|");
                if (v[i] < 10)
                    printf(" ");
                printf("%hu", v[i]);
            }
        }
    }
    printf("|\n");
}

// Return the channel to be used, given a pair {ASN;chOff}.
uint16_t compute_frequency(const unsigned long long int ASN, const uint16_t ch_off)
{
    uint16_t index = (uint16_t) ((ASN + (unsigned long long int) ch_off) % (unsigned long long int) N_C);
    return hopping_sequence[index];
}

// Print the frequencies used at each
void print_frequencies(const unsigned long long int base_ASN, const uint16_t* shuffled_chOffSchedule, const uint16_t size)
{
    int i;
    uint16_t f;
    
    for (i = 0; i < size; i++) {
        if (chOff_schedule[i] == N_C) // timeslot not used
            printf("| -");
        else {
            printf("|");
            f = compute_frequency(base_ASN + i, shuffled_chOffSchedule[i]);
            if (f < 10)
                printf(" ");
            printf("%hu", f);
        }
    }
    printf("|\n");
}

int main(int argc, char **argv)
{
    int i, j;
    unsigned long long int current_ASN = starting_ASN;
    uint16_t* shuffled_timeslot_schedule = malloc(N_S * sizeof(uint16_t));
    uint16_t* shuffled_chOff_schedule = malloc(N_S * sizeof(uint16_t));
    
    // 'z_s' grows of (N_S - 1) at each slotframe
    unsigned long long int z_s = (N_S - 1) * (current_ASN / N_S);

    // 'z_c' grows of (N_C - 1) at each slotframe
    unsigned long long int z_c = (N_C - 1) * (current_ASN / N_S);

    //printf("unsigned long long int: %lu\n", sizeof(unsigned long long int));
    //printf("uint64_t: %lu\n", sizeof(uint64_t));
    //printf("short int: %lu\n", sizeof(short int));
    //printf("uint16_t: %lu\n", sizeof(uint16_t));
    //printf("unsigned char: %lu\n", sizeof(unsigned char));
    
    printf("\nOriginal timeslot schedule:        ");
    print_schedule(timeslot_schedule, N_S, 0);
    
    printf("Original channel offset schedule:  ");
    print_schedule(chOff_schedule, N_S, 1);
    
    for (i = 0; i < rounds; i++) {
        printf("\n-----------------------------------\n\n");
        printf("Start round %d of %d\n", i+1, rounds);
        printf("Slotframe starts with: ASN %llu; z_s %llu; z_c %llu\n\n", current_ASN, z_s, z_c);
        
        memset(shuffled_timeslot_schedule, 0, N_S);
        memcpy(shuffled_timeslot_schedule, timeslot_schedule, N_S * sizeof(uint16_t));
        memset(shuffled_chOff_schedule, 0, N_S);
        memcpy(shuffled_chOff_schedule, chOff_schedule, N_S * sizeof(uint16_t));
        
        printf("Start shuffling the time offsets\n\n");
        shuffle_timeslots(shuffled_timeslot_schedule,
                          N_S, &z_s, shuffled_chOff_schedule);
        
        printf("Intermediate schedules\n\n");
        print_schedule(shuffled_timeslot_schedule, N_S, 0);
        print_schedule(shuffled_chOff_schedule, N_S, 1);
        printf("\n**********\n\n");
        
        printf("Start shuffling the channel offset schedule\n\n");
        shuffle_chOff(shuffled_chOff_schedule, N_S, &z_c);

        printf("*Next* slotframe starting with ASN %llu --> Shuffled timeslot schedule:        ", current_ASN + N_S);
        print_schedule(shuffled_timeslot_schedule, N_S, 0);
        
        printf("*Next* slotframe starting with ASN %llu --> Shuffled channel offset schedule:  ", current_ASN + N_S);
        print_schedule(shuffled_chOff_schedule, N_S, 1);
        
        printf("*Next* slotframe starting with ASN %llu --> Shuffled frequencies schedule:     ", current_ASN + N_S);
        print_frequencies(current_ASN + N_S, shuffled_chOff_schedule, N_S);
        
        current_ASN += N_S;
    
    }

    free(shuffled_timeslot_schedule);
    free(shuffled_chOff_schedule);
    
    return 0;
    
}
